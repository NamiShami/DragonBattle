﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary;

namespace DragonBattle
{
    class Program
    {
        static void Main(string[] args)
        {
            Warrior warrior = new Warrior("Aragorn");
            Archer archer = new Archer("Legolas");
            Mage mage = new Mage("Gandalf");

            var dragon = new Dragon
            {
                Strength = 7,
                Intelligence = 20,
                Agility = 15,
                MaxManaPoints = 100,
                CurrentManaPoints = 100,
                DamageDealt = 25,
                DamageResistance = 17,
                CurrentHealthPoint = 200,
                MaxHealthPoint = 200,
                Name = "Bambi"
            };
            dragon.OnFireBreath += (x) =>
            {
                archer.DamageTaken(x);
                mage.DamageTaken(x);
                warrior.DamageTaken(x);
            };

            archer.RaiseExpirience(10);
            warrior.RaiseExpirience(10);
            ExtentionClass.RaiseExpirience(mage, 10);

            do
            {

                
                OneRound(warrior, archer, mage, dragon);
                warrior.RaiseExpirience(20);
                archer.RaiseExpirience(20);
                mage.RaiseExpirience(20);
                dragon.RaiseExpirience(20);

                if (dragon.IsDead())
                {
                    Console.WriteLine("Drużyna dzielnych wojowników pokonała potężnego smoka");
                    break;
                }
                else if (mage.IsDead() && warrior.IsDead() && archer.IsDead())
                {
                    Console.WriteLine("Drużyna dzielnych wojowników została pokonana");
                    break;
                }


            } while (!warrior.IsDead() && !mage.IsDead() && !archer.IsDead() || !dragon.IsDead());


        }


        public static void OneRound(Warrior w, Archer a, Mage m, Dragon d)
        {

            
            if (!m.IsDead())
            {
                try
                {
                    d.DamageTaken(m.CastMagic(13));
                    Console.WriteLine(m);
                }
                catch (ManaPointException e)
                {
                    Console.WriteLine(m);
                    Console.WriteLine(e.Message);
                    m.ManaRegeneration(15);
                    d.DamageTaken(m.DamagePerRound);

                }

            }
            if (!w.IsDead())
            {
                d.DamageTaken(w.DamagePerRound);
                Console.WriteLine(w);
            }
            if (!a.IsDead())
            {
                d.DamageTaken(a.DamagePerRound);
                Console.WriteLine(a);
            }

            if (!d.IsDead())
            {
                d.FireBreath();
                Console.WriteLine(d);
            }
            Console.WriteLine();
        }
    }
}
