﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DragonBattle
{
    public static class ExtentionClass
    {
        public static void PotionOfFullHealling(this Character character)
        {
            character.CurrentHealthPoint = character.MaxHealthPoint;
        }
        public static bool IsDead(this Character character)
        {
            return character.CurrentHealthPoint <= 0;
        }
        public static void RaiseExpirience(this Character character, int expirience)
        {
            
            if (character.ExpiriencePoints + expirience >= 100)
            {
                character.ExpiriencePoints = character.ExpiriencePoints - 100;
                character.LvlUp();
            }else
            {
                character.ExpiriencePoints += expirience;
            }
        }
        public static void ManaRegeneration(this Mage mage, int additionalMana)
        {
            if(mage.CurrentManaPoints + additionalMana > mage.MaxManaPoints)
            {
                mage.CurrentManaPoints = mage.MaxManaPoints;
            }else
            {
                mage.CurrentManaPoints += additionalMana;
            }
        }
        public static void UnArming(this Character character, int damage)
        {
            character.DamageDealt -= damage;
        }
        public static void UnShielding(this Character character, int arrmor)
        {
            character.DamageResistance -= arrmor;
        }
    }
}
