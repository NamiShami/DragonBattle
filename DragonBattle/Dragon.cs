﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DragonBattle
{
    public delegate void FireBreathEvent(int x);
    public class Dragon : Character, Magic
    {
        public int CurrentManaPoints { get; set; }
        public int MaxManaPoints { get; set; }

        public event FireBreathEvent OnFireBreath;

        public int CastMagic(int MP)
        {
            CurrentManaPoints -= 20;
            FireBreath();
            return DamageDealt * Strength / 4;

        }

        public void Fire()
        {
            Console.WriteLine("Wziuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu.................");
        }

        public void FireBreath()
        {
            var damage = DamageDealt * Strength / 4;
            OnFireBreath?.Invoke(damage);
            Fire();
            Console.WriteLine($"Smok: {Name} zionął ogniem i zadał {damage} obrażeń wszystkim");
        }

    }
}
