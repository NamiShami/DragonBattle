﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Archer : Character
    {
        public int DodgeChance { get; set; }
        public Archer(string name, int lvl, int expiriencePoints, int strength, int agility, int inteligence, int maxHealthPoint, int currentHealthPoint, int damageDealt, int damageResistance)
            : base(name, lvl, expiriencePoints, strength, agility, inteligence, maxHealthPoint, currentHealthPoint, damageDealt, damageResistance)
        {
            DodgeChance = 10;
        }

        public Archer(string name)
            : base(name, 1, 0, 10, 15, 8, 100, 100, 28, 15)
        {
            DodgeChance = 10;
        }
        public override string ToString()
        {
            return base.ToString() + $" szansa na unik {DodgeChance}";
        }
        public override void DamageTaken(int dam)
        {
            var r = new Random();
            var throwADice = r.Next(1, 100);
            if (DodgeChance < throwADice) {
                base.DamageTaken(dam);
            }
        }
        public override void LvlUp()
        {
            base.LvlUp();
            Strength += 2;
            Intelligence+= 2;
            Agility += 5;
            DodgeChance += 5;

        }
    }
}
