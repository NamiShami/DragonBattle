﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Mage : Character, Magic
    {
        public  int MaxManaPoints { get; set; }
        public int CurrentManaPoints { get; set; }
        public Mage(string name, int lvl, int expiriencePoints, int strength, int agility, int inteligence, int maxHealthPoint, int currentHealthPoint, int damageDealt, int damageResistance)
            : base(name, lvl, expiriencePoints, strength, agility, inteligence, maxHealthPoint, currentHealthPoint, damageDealt, damageResistance)
        {
            MaxManaPoints = 75;
            CurrentManaPoints = 75;
        }

        public Mage(string name)
           : base(name, 1, 0, 8, 10, 15, 100, 100, 16, 13)
        {
            MaxManaPoints = 75;
            CurrentManaPoints = 75;
        }

        public override string ToString()
        {
            return base.ToString() + $" maxymalne punkty many {MaxManaPoints}, aktualne punkty many {CurrentManaPoints}";
        }
        public override void LvlUp()
        {
            base.LvlUp();
            Strength += 2;
            Agility += 2;
            Intelligence += 5;
            MaxManaPoints += 25;

        }

        public int CastMagic(int MP)
        {
            if(CurrentHealthPoint-MP < 0)
            {
                throw new ManaPointException("Za mało punktów many! Wypij potke");
            }
            CurrentManaPoints -= MP;
            return DamageDealt * 3;
        }
    }
}
