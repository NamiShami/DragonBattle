﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Warrior : Character
    {
        public static int AttacksPerRound { get; set; }
        public Warrior(string name, int lvl, int expiriencePoints, int strength, int agility, int inteligence, int maxHealthPoint, int currentHealthPoint, int damageDealt, int damageResistance)
            : base( name, lvl, expiriencePoints, strength, agility, inteligence, maxHealthPoint, currentHealthPoint, damageDealt, damageResistance) => AttacksPerRound = 2;

        public Warrior(string name)
            : base(name, 1, 0, 15, 10, 8, 100, 100, 16, 22) => AttacksPerRound = 2;

        public override string ToString()
        {
            return base.ToString() + $" Liczba ataków {AttacksPerRound}";
        }

        public override int DamagePerRound => base.DamagePerRound * AttacksPerRound;

        public override void LvlUp()
        {
            base.LvlUp();
            Agility += 2;
            Intelligence += 2;
            Strength += 5;
            if (Lvl % 30 == 0) { 
                AttacksPerRound += 1;
            }
        }
    }

}
