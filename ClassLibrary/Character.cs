﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public abstract class Character : IComparable<Character>
    {
        public string Name { get; set; }
        public int Lvl { get; set; }
        public int ExpiriencePoints { get; set; }
        public int Strength { get; set; }
        public int Agility { get; set; }
        public int Intelligence { get; set; }
        public int MaxHealthPoint { get; set; }
        public int CurrentHealthPoint { get; set; }
        public  int DamageDealt { get; set; }
        public int DamageResistance { get; set; }
        public virtual int DamagePerRound
        {
            get { return DamageDealt; }
        }
        public Character(string name, int lvl, int expiriencePoints, int strength, int agility, int inteligence, int maxHealthPoint, int currentHealthPoint, int damageDealt, int damageResistance)
        {
            Name = name;
            Lvl = lvl;
            ExpiriencePoints = expiriencePoints;
            Strength = strength;
            Agility = agility;
            Intelligence = inteligence;
            MaxHealthPoint = maxHealthPoint;
            CurrentHealthPoint = currentHealthPoint;
            DamageDealt = damageDealt;
            DamageResistance = damageResistance;
        }

        public Character()
        {

        }

        public override string ToString()
        {
            return $"{Name},  lvl {Lvl}, " +
                $"siła {Strength}, zręczność {Agility}, inteligencja {Intelligence}, " +
                $"obecne punkty zdrowia {CurrentHealthPoint}, max punkty zdrowia {MaxHealthPoint}";
        }


        public void EquipArmor(int armmor)
        {
            DamageResistance += armmor;
        }
        public void EquipWeapon(int damage)
        {
            DamageDealt += damage;
        }

        public virtual void DamageTaken(int dam)
        {
            CurrentHealthPoint -= (dam - DamageResistance);
        }

        public virtual void LvlUp()
        {
            Lvl++;
            ExpiriencePoints = 0;
            MaxHealthPoint += 50;
            DamageDealt += 5;
            DamageResistance += 5;
        }

        public int CompareTo(Character other)
        {
            return Lvl.CompareTo(other.Lvl);
        }
    }
}
