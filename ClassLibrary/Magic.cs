﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public interface Magic
    {
        int CastMagic(int MP);
        int CurrentManaPoints { get; set; }
        int MaxManaPoints { get; set; }
    }

    [Serializable]
    public class ManaPointException : Exception
    {
        public ManaPointException()
        {
        }

        public ManaPointException(string message) : base(message)
        {
        }

        public ManaPointException(string message, Exception inner) : base(message, inner)
        {
        }

        protected ManaPointException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }



    }

}
